/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h2t1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 28.5.2018
 */
public class Dog {
    
    private String name;
    private String says;
    
    /*
    Buildaus funktio.
    Asetetaan koiralle nimi
    Annetaan koiralle oletus sanonta "Much wow!"
    */
    public Dog() {
        setName();
        says = "Much wow!";
        
        System.out.println("Hei, nimeni on " + name + "!");
        
        setPunchline();
    }
    
    /*
    Asetetaan koiralle nimi.
    Jos koiralle ei anneta nimeä, koiran nimeksi laitetaan "Doge"
    */
    final void setName() {
        String newName;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Anna koiralle nimi: ");
        
        try {
            newName = br.readLine();
        } catch (IOException ex) {
            newName = "Doge";
        }
        
        if (newName.trim().isEmpty()) {
            newName = "Doge";
        }
        
        name = newName;
    }
    
    /*
    Kysytään mitä koira sanoo ja tallennetaan se says-muuttujaan.
    Jos käyttäjä ei anna mitään, tulostetaan "Much wow!", ja kysytään uudelleen
    */
    final void setPunchline() {
        String newPunchline;
        String lause;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Mitä koira sanoo: ");
        
        try {
            lause = br.readLine();
        } catch (IOException ex) {
            lause = "";
        }
        

        
        says = lause;
    }
    
    /*
    Koiran puhe funktio.
    Tulostetaan koiran sanonta näytölle
    */
    void speak() {
        System.out.println(name + ": " + says);
        
        Scanner s = new Scanner (lause);
        
        while (s.hasNext()) {
            if (s.hasNextInt()) {
                System.out.println("Such integer: " + s.next());
            }
            
            if (s.hasNextBoolean()) {
                System.out.println("Such boolean: " + s.next());
            }
        }
        
    }
    

}
