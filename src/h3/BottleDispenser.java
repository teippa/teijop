/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 29.5.2018
 */
public class BottleDispenser {
    
    private int bottles;
    private double money;

    // The array for the Bottle-objects
    private ArrayList<Bottle> bottle_array;




    /**
     * Luodaan lista pulloista jotka ovat koneessa ja alustetaan muuttujat 
    */
    public BottleDispenser() {

        bottles = 6;
        money = 0;

        // Initialize the array1

        bottle_array = new ArrayList();

        // Add Bottle-objects to the array
        
        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.3, 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3, 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3, 0.5, 1.95));

        /*
        for(int i = 0;i<bottles;i++) {
            
            // Use the default constructor to create new Bottles
            bottle_array.add(new Bottle());

        }
        */

    }



    /**
     * Lisätään yksi raha koneeseen
    */
    public void addMoney() {

        money += 1;
        
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }



    /**
     * Ostetaan pullo. Käytetään rahaa ja poistetaan pullo koneesta 
    */
    public void buyBottle(int bottleIndex) {
        
        if (!bottle_array.isEmpty()) {
            
            
            Bottle nextBottle = bottle_array.get(bottleIndex-1);
            double bottlePrice = nextBottle.getPrice();


            if ( money >= bottlePrice ) {
                System.out.println("KACHUNK! " + nextBottle.getName() + " tipahti masiinasta!");
                bottle_array.remove(bottleIndex-1);
                bottles -= 1;
                money -= bottlePrice;
            } else {
                System.out.println("Syötä rahaa ensin!");
            } 
        } else {
            System.out.println("Pullot loppuivat!");
        }
    }

    /**
     * Poistetaan kaikki rahat koneesta 
    */
    public void returnMoney() {
        
        String message = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€", money);

        System.out.println(message);
        
        money = 0;

    }
    
    public void listBottles() {
        
        int i = 1;
        for (Bottle btl : bottle_array) {
            System.out.println(i+". Nimi: " + btl.getName());
            System.out.println("\tKoko: " + btl.getSize() + "\tHinta: " + btl.getPrice());
            
            i++;
        }
    }
    
    public void printUI() {
        
        System.out.println("\n*** LIMSA-AUTOMAATTI ***");
        System.out.println("1) Lisää rahaa koneeseen");
        System.out.println("2) Osta pullo");
        System.out.println("3) Ota rahat ulos");
        System.out.println("4) Listaa koneessa olevat pullot");
        System.out.println("0) Lopeta");
    }
    
    public int getInput() {
        String inputStr;
        int inputInt;
                
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Valintasi: ");
        
        inputStr = scan.nextLine();
        
        try {
            inputInt = Integer.parseInt(inputStr);
        } catch (NumberFormatException e) {
            inputInt = -1;
        }
            
        return inputInt;
    }
}
