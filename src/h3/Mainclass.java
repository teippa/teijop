/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h3;


/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 29.5.2018
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean run = true;
        int valinta;
        int bottleIndex;
        
        
        BottleDispenser kone;
        kone = new BottleDispenser();
        
        while (run) {
            kone.printUI();
            
            valinta = kone.getInput();
            
            if (valinta == 1) {
                kone.addMoney();
            } else if (valinta == 2) {
                
                kone.listBottles();
                bottleIndex = kone.getInput();
                
                kone.buyBottle(bottleIndex);
            } else if (valinta == 3) {
                kone.returnMoney();
            } else if (valinta == 4) {
                kone.listBottles();
            } else if (valinta == 0) {
                run = false;
            } else {
                System.out.println("Tuntematon syöte.");
            }
            
        }
        
        
    }
}
